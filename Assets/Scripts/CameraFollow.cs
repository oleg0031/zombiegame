using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform player;
    [SerializeField] private float minX, maxX;
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
    }
    
    void LateUpdate()
    {
        if (!player)
            return;
        if (player.position.x > minX && player.position.x < maxX)
            transform.position = new Vector3(player.position.x, 0f, -10f);
    }
}
