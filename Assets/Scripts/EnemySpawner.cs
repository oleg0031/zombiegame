using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] 
    private Transform leftSpawner, rightSpawner;

    [SerializeField] 
    private List<GameObject> enemies;

    private int randomEnemyIndex, randomSide, randomSpeed;
    void Start()
    {
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1f, 5f));
            randomEnemyIndex = Random.Range(0, enemies.Count);
            randomSide = Random.Range(0, 2);
            randomSpeed = Random.Range(4, 10);
            GameObject enemy = enemies[randomEnemyIndex];
            if (randomSide == 0)
            {
                enemy.transform.position = leftSpawner.position;
                enemy.GetComponent<Enemy>().monsterSpeed = randomSpeed;
                enemy.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                enemy.transform.position = rightSpawner.position;
                enemy.GetComponent<Enemy>().monsterSpeed = -randomSpeed;
                enemy.GetComponent<SpriteRenderer>().flipX = true;
            }
            Instantiate(enemy);
        }
    }
}
