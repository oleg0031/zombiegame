using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float moveForce;
    [SerializeField] private float jumpForce;
    private float _movementX;
    private Rigidbody2D _rigidbody2D;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    private const string WalkAnimation = "Walk";
    private const string GroundTag = "Ground";
    private const string EnemyTag = "Enemy";
    private bool isGrounded;
    
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        InputListener();
        AnimatePlayer();
    }

    void InputListener()
    {
        _movementX = Input.GetAxisRaw("Horizontal");
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            _rigidbody2D.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
            isGrounded = false;
        }
    }

    private void AnimatePlayer()
    {
        transform.position += new Vector3(_movementX, 0f, 0f) * moveForce * Time.deltaTime;
        if (_movementX > 0f)
        {
            _animator.SetBool(WalkAnimation, true);
            _spriteRenderer.flipX = false;
        }
        else if (_movementX < 0f)
        {
            _animator.SetBool(WalkAnimation, true);
            _spriteRenderer.flipX = true;
        }
        else
        {
            _animator.SetBool(WalkAnimation, false);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag(GroundTag))
            isGrounded = true;
        else if (col.gameObject.CompareTag(EnemyTag))
        {
            Destroy(gameObject);
        }
    }
    
}
