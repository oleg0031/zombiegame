using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public int monsterSpeed;
    private const string EnemyTag = "Enemy";

    void FixedUpdate()
    {
        Vector3 tmpPos = transform.position;
        tmpPos.x += monsterSpeed * Time.deltaTime;
        transform.position = tmpPos;
    }
}
